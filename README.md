# @ddlab/tuner

A guitar tuner in a browser from [@ddlab](https://ddlab.tk)

For detailed explanation how it works see **"The Tuning Algorithm"** section.

To embed it in your javascript project read the following section.

## Package Usage

`npm i @ddlab/tuner`;

Initialize a tuner widget:
```typescript
import {initTuner} from '@ddlab/tuner';
initTuner(document.body);
```

For now styles have to be added manually, e.g.:
```html
<link rel="stylesheet" href="node_modules/@ddlab/tuner/dist/index.css" >
```

### Other Examples

Analyzing static data:
```typescript
import {getTuningInfo} from '@ddlab/tuner';

const deltaFreq = sampleRate / fftSize;
const {noteStr, cents, isInTune} = getTuningInfo(freqData, deltaFreq);
```

Continuously analyzing mic stream
```typescript
import {getTuningInfo, initAudio} from '@ddlab/tuner';

const {getFreqData, deltaFreq} = await initAudio(); // this must go after some user event
const loop = () => {
    const {noteStr, cents, isInTune} = getTuningInfo(getFreqData(), deltaFreq);
    // ...
    requestAnimationFrame(loop);
};
```

> **Warning:** initialization (`initAudio`) has to occur after some user event due to 
[browsers' autoplay policies](https://developers.google.com/web/updates/2017/09/autoplay-policy-changes#webaudio). 
For example: `addEventListener('keypress', () => initTuner(document.body));`

## The Tuning Algorithm

To find out how many cents a played note is out-of-tune, the following steps has to be made:

1. [Get a frequency spectrum](./docs/1.GetFreqSpectrum.md)
2. [Find fundamental peak freq. (in 50-400Hz range) in a spectrum](./docs/2.FindFundamentalFreq.md)
3. [Find harmonic peaks frequencies in a spectrum](./docs/3.FindHarmonics.md)
4. [Calculate the harmonic average distance to nearest in-tune note](./docs/4.DetermineOutOfTuneLevel.md)
5. Convert the diff to cents and represent it


## References
* http://amath.colorado.edu/pub/matlab/music/MathMusic.pdf
* http://www.tedknowlton.com/resume/CCPPT.htm
* https://jakevdp.github.io/blog/2013/08/28/understanding-the-fft/
* [./docs/misc/GuitarFretboardFrequencies.md](./docs/misc/GuitarFretboardFrequencies.md)

## License
[GNU GENERAL PUBLIC LICENSE](./LICENSE)
___
&copy; Dominykas Dukštas ([@ddlab](https://ddlab.tk)) 2019