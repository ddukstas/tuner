import { initTuner } from "../src/initTuner";
import css from './index.scss';
import 'markdown-it-highlight/dist/index.css'
import readmeText from '../README.md';
import sections from '../docs/*.md';
import bSpectreImgPath from '../docs/misc/b-spectre.jpg';

const initApp = async (parent: Element) => {
    const headerEl = document.createElement('header');
    headerEl.classList.add(css.header);
    parent.appendChild(headerEl);

    const tuner = await initTuner(headerEl);

    const init = async (e) => {
        e.preventDefault();
        removeEventListener('keypress', init);

        tuner.start();

        addEventListener('keypress', e => {
            switch(e.code) {
                case 'Space':
                    e.preventDefault();
                    return tuner.toggle();
                case 'KeyV':
                    return tuner.toggleView();
                case 'KeyS':
                    return tuner.toggleSound();
            }
        });
    };

    addEventListener('keypress', init);

    const contentEl = document.createElement('main');
    contentEl.classList.add(css.content);
    parent.appendChild(contentEl);

    let headingStr = '<h2>References</h2>';
    let [overviewText, footerText] = 
        readmeText.html.split('<h2>References</h2>');
    footerText = headingStr + footerText;

    const overviewEl = document.createElement('article');
    overviewEl.innerHTML = overviewText;
    contentEl.appendChild(overviewEl);

    overviewEl.querySelectorAll('a').forEach(el => {
        const href = el.getAttribute('href');
        const match = href.match('/docs/(.+).md$');
        // const docPath = match && `/docs/${match[1]}.md`;
        const sectionHash = match && `#${match[1]}`;

        if (sectionHash) {
            // const repoPath = 'https://gitlab.com/ddukstas/tuner/blob/master';
            // el.setAttribute('href', repoPath + docPath);
            // el.setAttribute('target', '_blank');

            el.setAttribute('href', sectionHash);
        }
    });

    Object.keys(sections).sort().forEach(key => {
        const el = document.createElement('article');
        el.id = key;
        el.innerHTML = sections[key].html;

        el.querySelectorAll('img').forEach(img => {            
            if (img.getAttribute('src') === './misc/b-spectre.jpg') {
                img.setAttribute('src', bSpectreImgPath);
                img.classList.add(css.docImage);
            }
        });
            
        contentEl.appendChild(el);
    });
    
    const footerEl = document.createElement('section');
    footerEl.innerHTML = footerText;
    const repoPath = 'https://gitlab.com/ddukstas/tuner/blob/master';
    footerEl.querySelectorAll('a').forEach(el => {
        const href = el.getAttribute('href');
        const isAbsolute = !!href.match(/http(s?):/);
        if (!isAbsolute) {
            el.setAttribute(
                'href', 
                repoPath + el.getAttribute('href').substr(1)
            );
        }
        el.setAttribute('target', '_blank');
    });

    contentEl.appendChild(footerEl);
}

initApp(document.body);
