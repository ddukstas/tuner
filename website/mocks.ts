import {mock} from '../src/utils/__tests__/mocks/data.mock';
import { initFreqGraphView } from "../src/components/Tuner/initFreqGraphView";
import { getHarmonics } from "../src/utils/freq.utils";

const drawMock = (data: Uint8Array, title: string) => {
    const titleEl = document.createElement('h3');
    titleEl.innerHTML = title;
    document.body.appendChild(titleEl);
    initFreqGraphView(document.body).draw(
        data, 
        getHarmonics(data, mock.deltaFreq)
    );
};

drawMock(mock.aStringData, 'A string');
drawMock(mock.bStringData, 'B string');
drawMock(mock.doubleSineData, 'Double Sine');
drawMock(mock.loudNoiseData, 'Loud Noise');
