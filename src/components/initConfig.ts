interface ITunerConfig {
    isAnalogView: boolean;
    isSoundEnabled: boolean;
}

const DEFAULT_CONFIG: ITunerConfig = {
    isAnalogView: true,
    isSoundEnabled: true
};

const LOCAL_STORAGE_KEY = '@ddlab/tuner';

export const initConfig = () => {
    let config: ITunerConfig;
    const item = localStorage.getItem(LOCAL_STORAGE_KEY);
    if (item) {
        config = JSON.parse(item);
    } else {
        config = {...DEFAULT_CONFIG};
        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(config));
    }

    const get = (): ITunerConfig => config;

    const set = (changes: Partial<ITunerConfig>) => {
        Object.assign(config, changes);
        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(config));
    };

    return {get, set};
};
