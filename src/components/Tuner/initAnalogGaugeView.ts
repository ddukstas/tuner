import { ITuningInfo } from "../../getTuningInfo";
import css from './styles/analogGauge.scss';
import { initFreqGraphView } from "./initFreqGraphView";

const getCentStr = (cents: number) => !isNaN(cents) ?
    `${cents >= 0 ? '+' : '-'}${Math.abs(cents)}¢` :
    ''

export const initAnalogGaugeView = (parent: Element) => {
    const container = document.createElement('div');
    container.classList.add(css.container);
    parent.appendChild(container);
    
    const freqGraph = initFreqGraphView(container);
    freqGraph.canvas.classList.add(css.graph);

    const noteEl = document.createElement('div');
    noteEl.classList.add(css.note);
    container.appendChild(noteEl);
    
    const centsEl = document.createElement('div');
    centsEl.classList.add(css.cents);
    container.appendChild(centsEl);
    
    const harmonicsEl = document.createElement('div');
    harmonicsEl.classList.add(css.harmonics);
    container.appendChild(harmonicsEl);

    const scaleEl = document.createElement('div');
    scaleEl.classList.add(css.scale);
    container.appendChild(scaleEl);

    const handEl = document.createElement('div');
    handEl.classList.add(css.hand);
    container.appendChild(handEl);

    return {
        update: (info: ITuningInfo, data: Uint8Array) => {
            if (noteEl.innerHTML !== info.noteStr) 
                noteEl.innerHTML = info.noteStr;
                    
            const harmonicsText = `h: ${info.harmonics.length}`;
            if (harmonicsEl.innerHTML !== harmonicsText) 
                harmonicsEl.innerHTML = harmonicsText;

            const centStr = getCentStr(info.cents);        
            if (centsEl.innerHTML !== centStr)
                centsEl.innerHTML = centStr;
            
            handEl.setAttribute(
                'style', 
                `transform: rotate(${info.cents}deg);`
            );

            freqGraph.draw(data, info.harmonics);
        },
        highlight: () => {
            handEl.classList.add(css.highlighted);
            setTimeout(() => handEl.classList.remove(css.highlighted), 250);
        },
        el: container
    }
};
