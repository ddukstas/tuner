const FILL_COLOR = '#333333';
const STROKE_COLOR = '#CCCCCC';

interface IGraphPoint {
    x: number; // %
    y: number; // %
    color?: string;
}
export type TGraphData = Array<IGraphPoint>;

interface IGraphConfig {
    width?: number; // px
    height?: number; // px
}

const getDefaultConfig = () => ({
    width: window.innerWidth / 2,
    height: window.innerWidth / 4
} as IGraphConfig);

export const initGraphView = (parent: Element, config: IGraphConfig = {}) => {
    config = Object.assign(getDefaultConfig(), config);
    const canvas = document.createElement('canvas');
    canvas.height = config.height;
    canvas.width = config.width;
    parent.appendChild(canvas);
    
    const midHeight = config.height / 2;
    const ctx = canvas.getContext('2d');
    ctx.lineWidth = 2;
    ctx.strokeStyle = STROKE_COLOR;

    const clear = () => {
        ctx.fillStyle = FILL_COLOR;
        ctx.fillRect(0, 0, config.width, config.height);
    };

    const drawPath = (data: TGraphData, color = STROKE_COLOR) => {        
        if (!data.length) return;

        ctx.lineWidth = 2;
        ctx.strokeStyle = color;

        ctx.beginPath();
        ctx.moveTo(0, midHeight);
        ctx.moveTo(data[0].x, data[0].y);
        data.forEach(({x, y}) => ctx.lineTo(x, y));
        ctx.stroke();
    }

    const drawPoints = (
        data: TGraphData, 
        color = STROKE_COLOR, 
        width: number = 6, 
        height: number = 6
    ) => {    
        if (!data.length) return;

        ctx.fillStyle = color;
        data.forEach(({x, y}) =>
            ctx.fillRect(x - width/2, y - height/2, width, height)
        );
    }

    return {clear, drawPath, drawPoints, canvas};
}
