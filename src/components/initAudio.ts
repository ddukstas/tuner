import {initTestPlayer} from '../utils/__tests__/mocks/initTestPlayer'

export interface IAudio  {
    deltaFreq: number;
    getTimeData: () => Uint8Array;
    getFreqData: () => Uint8Array;
    beep: () => void;
}

export const initAudio = async (): Promise<IAudio> => {
    const ctx = new AudioContext();
    const analyser = ctx.createAnalyser();
    analyser.fftSize = 32768;
    analyser.smoothingTimeConstant = 0.8;
    
    // const filePlayer = initTestPlayer();
    // const source = ctx.createMediaElementSource(filePlayer);
    // filePlayer.play();

    const stream = await navigator.mediaDevices.getUserMedia({audio: true});
    const source = ctx.createMediaStreamSource(stream);
    source.connect(analyser);

    // let isMicSetUp = false;
    // const setupMic = async () => {
    //     if (isMicSetUp) return;
        
    //     isMicSetUp = true;
    // };

    const timeData = new Uint8Array(analyser.frequencyBinCount);
    const getTimeData = () => {
        analyser.getByteTimeDomainData(timeData);
        return timeData;
    };

    const freqData = new Uint8Array(analyser.frequencyBinCount);
    const getFreqData = () => {
        analyser.getByteFrequencyData(freqData);
        return freqData;
    };

    const deltaFreq = ctx.sampleRate / analyser.fftSize

    const osc = ctx.createOscillator();
    osc.type = "sine";
    osc.frequency.value = 1500;
    osc.start();
    
    const beep = () => {
        osc.connect(ctx.destination);
        setTimeout(() => osc.disconnect(ctx.destination), 200);
    };

    return {
        deltaFreq,
        getTimeData, 
        getFreqData,
        beep
    };
};
