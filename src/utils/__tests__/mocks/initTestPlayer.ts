import bStringAudio from './bString.wav';

// used for testing purposes for convenience
export const initTestPlayer = () => {
    var filePlayer = new Audio();
    filePlayer.src = bStringAudio;
    filePlayer.controls = true;
    document.body.appendChild(filePlayer);
    return filePlayer;
};
