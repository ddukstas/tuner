import { isPeakIndex, interpolatePeak } from "../peak.utils"
import { mock } from "./mocks/data.mock";

describe('isPeakIndex', () => {
    it('return true for correct peak index in small array', () => {
        const d = new Uint8Array([0, 0, 255, 0, 0]);
        expect(isPeakIndex(d, 2, 1)).toBeTruthy();
    });

    
    it('return true for correct peak index in large array', () => {
        const d = new Uint8Array(mock.doubleSineData);
        expect(isPeakIndex(d, 300, 10)).toBeTruthy();
    });    
});

describe('interpolatePeak', () => {
    it('returns a whole number when adjacent values are equal', () => {
        const data = new Uint8Array([0, 100, 255, 100]);
        expect(interpolatePeak(data, 2)).toBe(2);
    });

    it('returns a point in between two indexes when one adjacent value is equal to peak value', () => {
        const data = new Uint8Array([0, 255, 255, 0]);
        expect(interpolatePeak(data, 2)).toBe(1.5);
    });

    it('returns a point in between two indexes when one adjacent value is bigger than another.', () => {
        const data = new Uint8Array([0, 100, 100, 50]);
        expect(interpolatePeak(data, 2)).toBe(1.8);
    });

    it('handles edges', () => {
        const data = new Uint8Array([0, 0, 50, 100]);
        expect(interpolatePeak(data, 3)).toBeCloseTo(2.6666);
    });


});
