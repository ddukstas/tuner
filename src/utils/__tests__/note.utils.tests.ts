import { getTuningInfo } from "../note.utils"
import { mock } from "./mocks/data.mock"

describe('getTuningInfo', () => {
    it('returns +1 cents for a A string data', () => {
        const i = getTuningInfo(mock.aStringData, mock.deltaFreq);

        expect(i.noteNb).toBe(12);
        expect(i.noteStr).toBe('A');
        expect(i.cents).toBe(1);
        expect(i.harmonics).toHaveLength(9);
        expect(i.isInTune).toBe(true);
    })
});
