import { isFirstOccurrence } from "./common.utils";

const BASE_NOTE_FREQ = 55;

export const getNearestNoteNb = (freq: number) => {
    const noteNum = 12 * (Math.log(freq / BASE_NOTE_FREQ) / Math.log(2));
    return Math.round(noteNum);
};

export const getNoteFrequency = (noteNum: number) => 
    Math.exp((noteNum / 12) * Math.log(2)) * BASE_NOTE_FREQ;

const NOTE_STRINGS = ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"];

export const getNoteStr = (note: number) => NOTE_STRINGS[note % 12] || '-';

export const getNoteStrFromFrequency = (frequency: number) => {
    const note = getNearestNoteNb(frequency);
    return getNoteStr(note);
};

export const getNotesNumbersFromFrequencies = (frequencies: Array<number>) =>
    frequencies
        .map(getNearestNoteNb)
        .map(note => note % 12)
        .filter(isFirstOccurrence);

export const getNoteNamesFromNoteNumbers = (noteNumbers: Array<number>) =>
    noteNumbers.map(noteNumber => NOTE_STRINGS[noteNumber]);

export const getCents = (freq1: number, freq2: number) =>
    Math.round(1200 * Math.log2(freq1 / freq2));

export const getCentsToNearestNote = (freq: number) => {
    const nearestFreq = getNoteFrequency(getNearestNoteNb(freq));
    return getCents(freq, nearestFreq);
};
