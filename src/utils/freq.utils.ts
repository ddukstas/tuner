import { getNearestNoteNb, getNoteFrequency } from "./note.utils";
import { getPeakIndex, getTreshold } from "./peak.utils";
import { lowpass, bandpass } from "./filter.utils";

export const indexToFreq = (i: number, deltaFreq: number) => 
    Math.round(i * deltaFreq);

export const freqToIndex = (i: number, deltaFreq: number) => 
    Math.round(i / deltaFreq);

const getNoteFrequencyRange = (freq: number) => {
    const note = getNearestNoteNb(freq);
    return [
        getNoteFrequency(note - 1),
        getNoteFrequency(note + 1)
    ];
}

export const getNoteIndexRange = (index: number, deltaFreq: number) => {
    const freq = indexToFreq(index, deltaFreq);
    return getNoteFrequencyRange(freq)
        .map(f => freqToIndex(f, deltaFreq));
}

export interface IHarmonic {
    order: number;
    index: number;
    freq: number;
    value: number;
    treshold: number;
}

export const getHarmonics = (
    data: Uint8Array, 
    deltaFreq: number, 
    maxOrder = 10
) => {    
    const cutoffI = freqToIndex(400, deltaFreq);
    const fundData = lowpass(data, cutoffI);
    const tresholdV = getTreshold(fundData);
    
    // TODO review the algorithm. Plain max value search fluctuates less than `getFirstPeakIndex` algorithm, however it doesn't necessary pick the fundamental.
    const peakI = getPeakIndex(fundData, tresholdV);
    if (peakI === -1) return [];
    
    const peakV = data[Math.round(peakI)];
    let harmonics: Array<IHarmonic> = [{
        order: 1,
        index: Math.round(peakI),
        freq: indexToFreq(peakI, deltaFreq),
        value: peakV,
        treshold: tresholdV
    }];
    
    for (let order = 2; order < maxOrder; order++) {
        const targetHarmonicI = Math.round(peakI * order);
        const [fromI, toI] = getNoteIndexRange(targetHarmonicI, deltaFreq);
        const hData = bandpass(data, fromI, toI + 1);
        const hTresholdV = peakV / 3;

        const subarrayPeakI = getPeakIndex(hData, hTresholdV);

        if (subarrayPeakI !== -1) {
            const index = fromI + subarrayPeakI;
            harmonics.push({
                order,
                index: Math.round(index),
                freq: indexToFreq(index, deltaFreq),
                value: data[Math.round(index)],
                treshold: hTresholdV
            }); 
        }
    }

    return harmonics;
};
