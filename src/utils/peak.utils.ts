import { avg } from "./common.utils";

export const getTreshold = (data: Uint8Array) => {
    const maxV = 256; // data.BYTES_PER_ELEMENT * 256
    const minV = 64;
    const diff = avg(data) - Math.min(...data);
    return Math.min(maxV, Math.max(diff * 3, minV));
};

export const interpolatePeak = (data: Uint8Array, i: number) => {
    if (i === -1) return -1;
    
    const prevV = data[i-1] || 0;
    const nextV = data[i+1] || 0;
    return (
        prevV*(i-1) + data[i]*i + nextV*(i+1)
    ) / (
        prevV + data[i] + nextV
    );
}

export const getPeakIndex = (
    data: Uint8Array, 
    tresholdV = getTreshold(data)
) => {
    if (data.length === 1) return 0;

    let peakI = -1;
    let peakV = -1;
    data.forEach((v, i) => {
        if (v > tresholdV && v > peakV) {
            peakI = i;
            peakV = v;
        }
    });

    return interpolatePeak(data, peakI);
}

export const isPeakIndex = (data: Uint8Array, i: number, maxDistance = 1) => {
    for (let d = 1; d <= maxDistance; d++) {
        if (!(data[i] >= data[i - d] && data[i] >= data[i + d])) {
            return false;
        }
    }
    
    return true;
};

export const getPeakIndexes =  (
    data: Uint8Array,
    maxDistance = 1
) => {
    const tresholdV = getTreshold(data);
    const indexes = [];
    data.forEach((v, i) => {
        if (v > tresholdV && isPeakIndex(data, i, maxDistance)) {
            indexes.push(interpolatePeak(data, i));
        }
    });

    return indexes;
};

export const getFirstPeakIndex = (
    data: Uint8Array,
    maxDistance = 1
) => {
    const tresholdV = getTreshold(data);
    const peakI = data.findIndex((v, i) => 
        v > tresholdV && 
        isPeakIndex(data, i, maxDistance)
    );
    return interpolatePeak(data, peakI); 
};
