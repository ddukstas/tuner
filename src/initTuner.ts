import { initAudio, IAudio } from "./components/initAudio";
import { getTuningInfo } from "./getTuningInfo";
import { freqToIndex } from "./utils/freq.utils";
import { lowpass } from "./utils/filter.utils";
import { initTunerView } from "./components/Tuner/initTunerView";
import { initConfig } from "./components/initConfig";
import { initContinuouslyTrueChecker } from "./utils/common.utils";

export const initTuner = async (parent: Element) => {  
    let audio: IAudio | null = null;
    let cutoffI: number | null = null;

    const getIsInTuneFor1sec = initContinuouslyTrueChecker(1000);

    const config = initConfig();

    const tunerView = initTunerView(parent, config.get().isAnalogView);

    const toggleView = () => {
        const isAnalogView = !config.get().isAnalogView
        config.set({isAnalogView});
        tunerView.setView(isAnalogView);
    };

    const toggleSound = () => {
        const isSoundEnabled = !config.get().isSoundEnabled
        config.set({isSoundEnabled});
    };

    let isRunning = false;
    
    const run = () => {
        const freqData = lowpass(audio.getFreqData(), cutoffI);
        const info = getTuningInfo(freqData, audio.deltaFreq);

        tunerView.update(info, freqData);
        
        if (getIsInTuneFor1sec(info.isInTune)) {
            if (config.get().isSoundEnabled) {
                audio.beep();
            }

            tunerView.highlight();
        }
        
        if (isRunning) {
            requestAnimationFrame(run);
        }
    }

    const start = async () => {
        isRunning = true;
        if (!audio) {
            tunerView.showLoader();
            audio = await initAudio();
            cutoffI = freqToIndex(2000, audio.deltaFreq);
            tunerView.hideTurnOnButton();
        }

        run();
    }

    const pause = () => {
        isRunning = false;
    }

    const toggle = () => isRunning ? pause() : start();

    tunerView.onTurnOnClick(start);

    return {start, pause, toggle, toggleView, toggleSound};
};
