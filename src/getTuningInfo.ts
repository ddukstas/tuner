import { avg } from "./utils/common.utils";
import { getHarmonics, IHarmonic } from "./utils/freq.utils";
import { getCentsToNearestNote, getNearestNoteNb, getNoteStr } from "./utils/note.utils";

export interface ITuningInfo {
    noteNb: number;
    noteStr: string;
    cents: number;
    harmonics: Array<IHarmonic>;
    isInTune: boolean;
}

export const getTuningInfo = (data: Uint8Array, deltaFreq: number) => {
    const hs = getHarmonics(data, deltaFreq);
    const centsList = hs.map(h => getCentsToNearestNote(h.freq));
    const noteNb = hs.length ? getNearestNoteNb(hs[0].freq) : NaN;
    const cents = Math.round(avg(centsList));
    return {
        noteNb,
        noteStr: getNoteStr(noteNb),
        harmonics: hs,
        cents,
        isInTune: Math.abs(cents) < 8
    } as ITuningInfo;
};
